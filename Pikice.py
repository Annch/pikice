
from tkinter import *

import random
from random import randint

# Funkcija, ki ustavi seznam vseh možnosti na začetku igre, ko računalnik ugiba barvno kombinacijo
def seznam_moznosti():
    seznam = []
    for i in range(6):
        for j in range(6):
            for k in range(6):
                for l in range(6):
                    seznam.append([i, j, k, l])
    return seznam

# Funkcija, ki vrne seznam vseh možnih odgovorov, ki ji lahko dobimo ob ugibanju
def seznam_odgovorov():
    seznam = [[0, 0], [1, 0], [3, 0], [0, 1], [0, 2],
              [0, 3], [0, 4], [1, 3], [2, 2], [1, 1],
              [1, 2], [2, 1], [4, 0], [2, 0]]
    return seznam

# Funkcija, ki naredi in vrne seznam s 4 naključno izbranimi barvami, na voljo je 6 različnih barv 
def nakljucni_seznam_barv():
    seznam = []
    for i in range(4):
        seznam.append(randint(0, 5))
    return seznam

# Funkcija, ki naredi in vrne seznam oblike xxyy
def premisljeni_seznam_barv():
    seznam = []
    barve = [0, 1, 2, 3, 4, 5]
    for i in range(2):
        a = random.choice(barve)
        seznam.append(a)
        seznam.append(a)
        barve.remove(a)
    return seznam

# Funkcija, ki naredi in vrne seznam s 4 najključno izbranimi barvami, ki pa se ne ponavljajo
def nakljucni_seznam_barv_brez_ponavljanj():
    seznam = []
    barve = [0, 1, 2, 3, 4, 5]
    for i in range(4):
        a = random.choice(barve)
        seznam.append(a)
        barve.remove(a)
    return seznam

# Funkcija, ki izračuna odgovor na poskus igralca
def odgovor(vzorec, poskus):
    odg = [0,0]
    # prva števka v odg je število črnih, druga pa število belih
    seznam_vzorec = [0, 0, 0, 0, 0, 0]
    seznam_poskus = [0, 0, 0, 0, 0, 0]
    for i in range(4):
        if poskus[i] == vzorec[i]:
            odg[0] += 1
        else:
            seznam_vzorec[vzorec[i]] += 1
            seznam_poskus[poskus[i]] += 1
    for i in range(6):
        a = min(seznam_vzorec[i], seznam_poskus[i])
        for j in range(a):
            odg[1] += 1
    return odg

# Funkcija, ki vrne seznam možnih kombinacij na podlagi računalnikovega poskusa in odgovora
def ostali(seznam, pos, odg):
    seznam_ostalih = []
    for i in seznam:
        if odgovor(i, pos) == odg:
            seznam_ostalih.append(i)
    return seznam_ostalih

# Funkcija, ki seznam barv spremeni v seznam s številkami
def barve_v_stevilke(seznam):
    slovar = {"yellow":0, "blue":1, "purple":2, "red":3, "green":4, "pink":5}
    for i in range(len(seznam)):
        seznam[i] = slovar[seznam[i]]
    return seznam

# Funkcija, ki seznam črnih, belih oz neutralnih spremeni v seznam s številkami
def crne_v_stevilke(seznam):
    slovar = {"black":0, "white":1, "#990000":2}
    a = seznam.count("balck")
    b = seznam.count("white")
    return [a, b]

# Funkcija, ki nam pretvori številke v barve
def pretvornik_crne(seznam):
    sez = []
    for i in range(seznam[0]):
        sez.append("black")
    for i in range(seznam[1]):
        sez.append("white")
    while len(sez) < 4:
        sez.append("#990000")
    return sez

# Funkcija, ki nam pretvori številke v barve
def pretvornik_barve(seznam):
    slovar = {0:"yellow", 1:"blue", 2:"purple", 3:"red", 4:"green", 5:"pink"}
    for i in range(len(seznam)):
        seznam[i] = slovar[seznam[i]]
    return seznam

class Pikice():
        
    def __init__(self, root):

        # Okvir, ki se prikaže na zaslonu, ko poženemo program oz začnemo z novo igro
        self.okvir_vhodni = Frame(root, bg="#660000", padx=100, pady=100) 
        self.okvir_vhodni.grid(column=1, row=1, sticky=W+E+N+S)

        label_vhodni = Label(self.okvir_vhodni, text="IZBERI IGRALCA:\n(tistega, ki bo ugibal)",
                             font=("Arial Black", 20), bg="#660000", fg="#FFFF99")
        label_vhodni.grid(column=3, row=0)

        label_kr_neki1 = Label(self.okvir_vhodni, height=7, bg="#660000")
        label_kr_neki1.grid(column=3, row=1)
        # zgornji "label" je namenjen lepši obliki

        # Gumba za določitev igralca oz tistega, ki ugiba barvno kombinacijo
        gumb_igralec = Button(self.okvir_vhodni, text="Uporabnik", font=("Arial Black", 20),
                              bg="#990000", fg="#FFFF99", command=lambda:self.igralec_uporabnik())
        gumb_igralec.grid(column=3, row=3, sticky=W+E)

        gumb_racunalnik = Button(self.okvir_vhodni, text="Računalnik", font=("Arial Black", 20),
                                 bg="#990000", fg="#FFFF99", command=lambda:self.igralec_racunalnik())
        gumb_racunalnik.grid(column=3, row=4, sticky=W+E)

        # Okvir, na katerem lahko izberemo težavnost igre
        # Uporavno le, ko uporabnik ugiba barvno kombinacijo
        self.okvir_tezavnost = Frame(root, bg="#660000", padx=100, pady=100)

        label_opis = Label(self.okvir_tezavnost, text="V barvni kombinaciji se barve:\n",
                             font=("Arial Black", 20), bg="#660000", fg="#FFFF99")
        label_opis.grid(column=2, row=1, columnspan=3)

        gumb_level1 = Button(self.okvir_tezavnost, text="LEVEL 1", font=("Arial Black", 20),
                             bg="#990000", fg="#FFFF99", command=lambda:self.level1())
        gumb_level1.grid(column=4, row=3, sticky=W+E)

        label_opis1 = Label(self.okvir_tezavnost, text="ne ponavljajo",
                             font=("Arial Black", 20), bg="#660000", fg="#FFFF99")
        label_opis1.grid(column=2, row=3, sticky=W)

        gumb_level2 = Button(self.okvir_tezavnost, text="LEVEL 2", font=("Arial Black", 20),
                                 bg="#990000", fg="#FFFF99", command=lambda:self.level2())
        gumb_level2.grid(column=4, row=5, sticky=W+E)

        label_opis2 = Label(self.okvir_tezavnost, text="lahko ponavljajo",
                             font=("Arial Black", 20), bg="#660000", fg="#FFFF99")
        label_opis2.grid(column=2, row=5, sticky=W)

        # Igralno polje
        self.okvir = Frame(root, bg="#660000", padx=50, pady=50)

        label_kr_neki2 = Label(self.okvir, width=1, bg="#660000")
        label_kr_neki2.grid(column=4, row=1)

        label_kr_neki3 = Label(self.okvir, height=1, bg="#660000")
        label_kr_neki3.grid(column=0, row=10)
        # zgornja dva "lable-a" sta namenjena zgolj lepši obliki

        # Spremenjljivka v kateri je shranjen igralez oz tisti, ki ugiba barvno kombinacijo
        self.igralec = StringVar()
        self.igralec.set("Uporabnik")

        # Spremenljivka, ki nam določa v katerem koraku igre smo
        self.vrstica = 0

        # Matrike, v katerih so spravljeni deli igralnega polja (levo, desno, vrorec - spodaj)
        self.m1 = []
        self.m2 = []
        self.m3 = []

        for i in range(6):
            l1 = []
            l2 = []
            for j in range(4):
                l1.append(Label(self.okvir, bg="#990000", width=7))
                l2.append(Label(self.okvir, bg="#990000", width=7))
            self.m1.append(l1)
            self.m2.append(l2)
            for k in range(4):
                self.m1[i][k].grid(column=k, row=i)
                self.m2[i][k].grid(column=(k+5), row=i)

        for i in range(4):
            self.m3.append(Label(self.okvir, bg="#990000", width=7))
            self.m3[i].grid(column=i, row=12)
            self.m3[i].config(bg="#660000")

        # Seznami za potrebe po zapomnitvi barv
        self.vzorec = []
        self.poskus = []
        self.crne = []
        
        # Barvni meniji
        self.barva1 = StringVar()

        self.menu1 = OptionMenu(self.okvir, self.barva1, "rumena", "modra", "vijolična", "rdeča", "zelena", "roza")
        self.menu1.config(bg="#990000", fg="#FFFFCC", activebackground="#FF6666", activeforeground="#FFFFCC", width=7)
        self.menu1.grid(column=0, row=11)
        
        self.barva2 = StringVar()

        self.menu2 = OptionMenu(self.okvir, self.barva2, "rumena", "modra", "vijolična", "rdeča", "zelena", "roza")
        self.menu2.config(bg="#990000", fg="#FFFFCC", activebackground="#FF6666", activeforeground="#FFFFCC", width=7)
        self.menu2.grid(column=1, row=11)
        
        self.barva3 = StringVar()

        self.menu3 = OptionMenu(self.okvir, self.barva3, "rumena", "modra", "vijolična", "rdeča", "zelena", "roza")
        self.menu3.config(bg="#990000", fg="#FFFFCC", activebackground="#FF6666", activeforeground="#FFFFCC", width=7)
        self.menu3.grid(column=2, row=11)
        
        self.barva4 = StringVar()

        self.menu4 = OptionMenu(self.okvir, self.barva4, "rumena", "modra", "vijolična", "rdeča", "zelena", "roza")
        self.menu4.config(bg="#990000", fg="#FFFFCC", activebackground="#FF6666", activeforeground="#FFFFCC", width=7)
        self.menu4.grid(column=3, row=11)
        
        # Gumb s katerim sprožimo nadaljenvanje igre
        self.nadaljuj = Button(self.okvir, text="OK", bg="#FFFF66", fg="#990000", font=("Arial Black", 15),
                               command=self.nadaljevanje)
        self.nadaljuj.grid(column=7, row=11, columnspan=2, rowspan=2, sticky=E)

        # "Label-i" in gumb za funkcijo pomoč
        self.pomoc = Button(self.okvir, text="?", bg="#FFFF66", fg="#990000", font=("Arial Black", 10),
                               command=self.namig)
        self.pomoc.grid(column=4, row=0)

        self.pomoc1 = Label(self.okvir, bg="#660000")
        self.pomoc1.grid(column=0, row=10)

        self.pomoc2 = Label(self.okvir, bg="#660000")
        self.pomoc2.grid(column=1, row=10)

        self.pomoc3 = Label(self.okvir, bg="#660000")
        self.pomoc3.grid(column=2, row=10)

        self.pomoc4 = Label(self.okvir, bg="#660000")
        self.pomoc4.grid(column=3, row=10)

        self.pomoc5 = Label(self.okvir, bg="#660000")
        self.pomoc5.grid(column=5, row=11, columnspan=3, sticky=W)
        
        for otrok in self.okvir.winfo_children():
            otrok.grid_configure(padx=5, pady=5)

    # Metoda, ki prehaja iz enega v drug okvir
    def prehod_okvir(self, okvir1, okvir2):
        okvir1.grid_forget()
        okvir2.grid(column=1, row=1, sticky=W+E+N+S)
        
    # Metoda, ki začne igro na način, kjer bo uporabnik ugibal barvno kombinacijo
    def igralec_uporabnik(self):
        self.igralec.set("Uporabnik")
        self.prehod_okvir(self.okvir_vhodni, self.okvir_tezavnost)

    # Metoda, ki začne igro na način, kjer bo uporabnik ugibal barvno kombinacijo, v kateri se barve ne ponavljajo
    def level1(self):
        self.vzorec = nakljucni_seznam_barv_brez_ponavljanj()
        self.igralec_uporabnik_zacetek()

    # Metoda, ki začne igro na način, kjer bo uporabnik ugibal barvno kombinacijo, v kateri se barve lahko ponavljajo
    def level2(self):
        self.vzorec = nakljucni_seznam_barv()
        self.igralec_uporabnik_zacetek()

    # Metoda, ki nadaljuje z začetkom igre, kjer bo uporabnik ugibal barvno kombinacijo
    def igralec_uporabnik_zacetek(self):
        self.prehod_okvir(self.okvir_tezavnost, self.okvir)
        self.odstrani_menu()
        self.zasenci_barve()
        self.pripni_label(11, 0, self.m3)
        self.ponastavi_barve()
        self.odstrani_label(self.m1[0])
        self.pripni_menu(0, self.m1)
        self.vse_kombinacije = seznam_moznosti()
        self.vsi_odgovori = seznam_odgovorov()
        self.pomoc.grid(column=4, row=0)

    # Metoda, ki začne igro na način, kjer bo računalnik ugibal barvno kombinacijo
    def igralec_racunalnik(self):
        self.igralec.set("Računalnik")
        self.prehod_okvir(self.okvir_vhodni, self.okvir)
        self.pomoc.grid_forget()
        self.vse_kombinacije = seznam_moznosti()
        self.vsi_odgovori = seznam_odgovorov()
        self.ponastavi_barve()
        self.odstrani_label(self.m3)
        self.pripni_menu(11, self.m3)

    # Metoda, ki odstani vse menije iz okvirja
    def odstrani_menu(self):
        self.menu1.grid_forget()
        self.menu2.grid_forget()
        self.menu3.grid_forget()
        self.menu4.grid_forget()

    # Metoda, ki pripne menije v določeno vrstico in na določen del polja
    def pripni_menu(self, vrstica, matrika):
        self.menu1.grid(column=0, row=vrstica)
        self.menu2.grid(column=1, row=vrstica)
        self.menu3.grid(column=2, row=vrstica)
        self.menu4.grid(column=3, row=vrstica)

    # Metoda, ki odstani "label-e" iz določenega dela polja
    def odstrani_label(self, matrika):
        matrika[0].grid_forget()
        matrika[1].grid_forget()
        matrika[2].grid_forget()
        matrika[3].grid_forget()

    # Metoda, ki pripne "label-e" iz določene vrstice in dela polja
    def pripni_label(self, vrstica, stolpec, matrika):
        matrika[0].grid(column=0+stolpec, row=vrstica)
        matrika[1].grid(column=1+stolpec, row=vrstica)
        matrika[2].grid(column=2+stolpec, row=vrstica)
        matrika[3].grid(column=3+stolpec, row=vrstica)
        self.pobarvaj_label(vrstica, stolpec, matrika)
        
    # Metoda, ki pobarva "label-e" iz določene vrstice in dela polja
    def pobarvaj_label(self, vrstica, stolpec, matrika):
        self.poskus = []

        matrika[0].config(bg=self.barva1.get())
        self.poskus.append(self.barva1.get())

        matrika[1].config(bg=self.barva2.get())
        self.poskus.append(self.barva2.get())

        matrika[2].config(bg=self.barva3.get())
        self.poskus.append(self.barva3.get())

        matrika[3].config(bg=self.barva4.get())
        self.poskus.append(self.barva4.get())

    # Metoda, ki vse spremenljivke oblike self.barva nastavi na rumeno
    def ponastavi_barve(self):
        self.barva1.set("rumena")
        self.barva2.set("rumena")
        self.barva3.set("rumena")
        self.barva4.set("rumena")

    # Metoda, ki vse spremenljivke oblike self.barva nastavi na sivo
    # Uporabno, ko računalnik sestavlja barvno kombinacijo in je uporabnik ne sme videti
    def zasenci_barve(self):
        self.barva1.set("grey")
        self.barva2.set("grey")
        self.barva3.set("grey")
        self.barva4.set("grey")

    # Metoda, ki pretvori barve v slovenščini v barve v angleščini, da program zna pobarvati "label-e"
    def slo_ang_barve(self):
        slovar_barv = {"rumena":"yellow", "rdeča":"red", "zelena":"green", "modra":"blue", "roza":"pink", "vijolična":"purple"}
        self.barva1.set(slovar_barv[self.barva1.get()])
        self.barva2.set(slovar_barv[self.barva2.get()])
        self.barva3.set(slovar_barv[self.barva3.get()])
        self.barva4.set(slovar_barv[self.barva4.get()])

    # Metoda, ki preverja, če je igralec postavil kakšno barvo na pravo mesto oz je uganil le barvo in pobarva ustezna polja
    def preveri(self):
        a = self.vzorec[:]
        b = self.poskus[:]
        b = barve_v_stevilke(b)
        self.crne = odgovor(a, b)
        self.crne = pretvornik_crne(self.crne)
        self.priredi_barve(self.crne)
        self.pobarvaj_label(self.vrstica, 5, self.m2[self.vrstica])

    # Metoda, ki preveri, če je igralec uganil barvno kombinacijo
    def preveri_konec(self):
        if "white" not in self.crne:
            if "#990000" not in self.crne:
                return True

    # Metoda, ki uporabniku ponudi možnost nove igre
    def konec_igre(self):
        self.nadaljuj.grid_forget()
        self.gumb_nova_igra = Button(self.okvir, text="NOVA IGRA", bg="#FFFF66", fg="#990000",
                                     font=("Arial Black", 13), command=self.nova_igra)
        self.gumb_nova_igra.grid(column=7, row=11, columnspan=2, rowspan=2, sticky=W+E)
        if self.igralec.get() == "Uporabnik":
            self.vzorec = pretvornik_barve(self.vzorec)
            self.priredi_barve(self.vzorec)
            self.pobarvaj_label(11, 0, self.m3)
            self.pomoc.grid_forget()
        else:
            self.hmmm.grid_forget()
        

    # Metoda, ki spremenljivkam oblike self.barva po vrsti priredi barve iz seznama
    def priredi_barve(self, seznam):
        self.barva1.set(seznam[0])
        self.barva2.set(seznam[1])
        self.barva3.set(seznam[2])
        self.barva4.set(seznam[3])

    # Metoda, ki začne igro od začetka
    def nova_igra(self):
        self.okvir.grid_forget()
        self.prehod_okvir(self.okvir, self.okvir_vhodni)
        self.vrstica = 0
        for i in range(0, 4):
            for j in range(0, 6):
                self.m1[j][i].config(bg="#990000")
                self.m2[j][i].config(bg="#990000")
            self.m3[i].config(bg="#990000")
        self.gumb_nova_igra.grid_forget()
        self.nadaljuj.grid(column=8, row=11)

    # Metoda, ki naredi in vrne seznam z vsebino barv
    # Uporabno, ko uporabnik sestavlja vzorec
    def naredi_seznam(self):
        seznam = []
        self.slo_ang_barve()
        seznam.append(self.barva1.get())
        seznam.append(self.barva2.get())
        seznam.append(self.barva3.get())
        seznam.append(self.barva4.get())
        return seznam

    # Metoda, ki vrne najboljšo izmed vseh možnih potez
    # Uporabno, ko računalnik ugotavnja katera poteza bi bila najboljša, ali pa uporabnik potrebuje pomoč
    def izberi_najboljso_potezo(self, seznam, poskus):
        u = poskus
        p = 0
        # garantiran profit
        if len(seznam) == 1:
            # ko je v seznamu ostalih le še ena (zadnja) možnost
            return seznam[0]
        for v in self.vse_kombinacije:
            q = 100000000000000
            for o in self.vsi_odgovori:
                t = ostali(seznam, v, o)
                d = len(seznam) - len(t)
                # najboljša poteza je tistka, ki nam množico preostalih možnih kombinacij najbolj zmanjša
                q = min(q, d)
            if q > p:
                # našli smo boljšo potezo
                u = v
                p = q
        return u

    # Metoda, ki prekine računanje računalnika, da na zaslon nariše kar je potrebno
    # Uporabno, ko računalnik ugiba barvno kombinacijo in daje vtis "naravnosti"
    def osvezi_okvir(self):
        self.okvir.update_idletasks()
        self.okvir.after(2000)

    # Metoda, ki ustavi "label", ki nam da občutek "živega" računalnika
    def label_hmmm(self):
        self.hmmm = Label(self.okvir, bg="#660000", fg="#FFFF99", font=("Arial Black", 13))
        self.hmmm.grid(column=6, row=11, columnspan=3, sticky=W)

    # Metoda, ki zgornji "label" osveži na način, ko računalnik preračunava potezo
    def label_hmmm_razmisljam(self):
        seznam = ["Razmišljam              ", "Hmmmmm...           ", "Kaj naj poskusim?!  "]
        self.hmmm.grid_forget()
        a = random.choice(seznam)
        self.hmmm.config(text=a)
        self.hmmm.grid(column=6, row=11, columnspan=3, sticky=W)

    # Metoda, ki je podobna prejšnji, le da računalnik čaka na odgovor
    def label_hmmm_cakam(self):
        seznam = ["Čakam odgovor      ", "In odgovor je...       ", "Upam, da sem zadel"]
        self.hmmm.grid_forget()
        a = random.choice(seznam)
        self.hmmm.config(text=a)
        self.hmmm.grid(column=6, row=11, columnspan=3, sticky=W)

    # Metoda, ki se izvede, ko igralec klikne na pomoč oz na gumb z "?"
    def namig(self):
        self.pomoc5.config(text="Preračunavam", fg="#FFFF99", font=("Arial Black", 13))
        self.osvezi_okvir()
        if self.vrstica == 0:
            a = premisljeni_seznam_barv()
        else:
            a = self.izberi_najboljso_potezo(self.prej_ostalih, self.prejsnji_poskus)
        self.namig_label(a)

    # Metoda, ki izpiše predlog, ko igralec klikne na pomoč oz na gumb z "?"
    def namig_label(self, poteza):
        slovar = {0:"rumena", 1:"modra", 2:"vijolična", 3:"rdeča", 4:"zelena", 5:"roza"}
        self.osvezi_okvir()
        self.pomoc1.config(text=slovar[poteza[0]], fg="#FFFF99", font=("Arial Black", 9))
        self.pomoc2.config(text=slovar[poteza[1]], fg="#FFFF99", font=("Arial Black", 9))
        self.pomoc3.config(text=slovar[poteza[2]], fg="#FFFF99", font=("Arial Black", 9))
        self.pomoc4.config(text=slovar[poteza[3]], fg="#FFFF99", font=("Arial Black", 9))
        self.pomoc5.config(text="            ")

    # Metoda, ki izbriše iz zaslona prejšnjo pomoč
    def namig_label_pozabi(self):
        self.pomoc1.config(text="          ")
        self.pomoc2.config(text="          ")
        self.pomoc3.config(text="          ")
        self.pomoc4.config(text="          ")
                
    # Metoda, ki se izvede ob pritisku na gumb OK
    def nadaljevanje(self):
        if self.igralec.get() == "Računalnik":
            self.nadaljuj.grid_forget()
            self.vzorec = self.naredi_seznam()
            self.odstrani_menu()
            self.pripni_label(11, 0, self.m3)
            self.label_hmmm()
            self.label_hmmm_razmisljam()
            self.osvezi_okvir()
            self.poskus = pretvornik_barve(premisljeni_seznam_barv())
            self.priredi_barve(self.poskus)
            self.pobarvaj_label(self.vrstica, 0, self.m1[self.vrstica])
            self.label_hmmm_cakam()
            self.osvezi_okvir()
            poskus = self.poskus[:]
            poskus = barve_v_stevilke(poskus)
            self.vzorec = barve_v_stevilke(self.vzorec)
            self.preveri()
            s = self.vse_kombinacije[:]
            o = odgovor(self.vzorec, poskus)
            s = ostali(s, poskus, o)
            while self.vrstica < 5:
                self.label_hmmm_razmisljam()
                self.osvezi_okvir()
                self.vrstica +=1
                u = self.izberi_najboljso_potezo(s, poskus)
                o = odgovor(self.vzorec, u)
                s = ostali(s, u, o)
                u = pretvornik_barve(u)
                self.priredi_barve(u)
                self.pobarvaj_label(self.vrstica, 0, self.m1[self.vrstica])
                self.label_hmmm_cakam()
                self.osvezi_okvir()
                self.preveri()
                poskus = barve_v_stevilke(u)
                if self.preveri_konec() == True:
                    self.hmmm.grid_forget()
                    self.vrstica = 5
        else:
            self.namig_label_pozabi()
            self.pomoc.grid(column=4, row=self.vrstica+1)
            self.odstrani_menu()
            self.prejsnji_poskus = barve_v_stevilke(self.naredi_seznam())
            if self.vrstica == 0:
                self.prej_ostalih = self.vse_kombinacije[:]
            self.seznam_ostalih = ostali(self.prej_ostalih, self.prejsnji_poskus, odgovor(self.vzorec, self.prejsnji_poskus))
            self.prej_ostalih = self.seznam_ostalih
            self.pripni_label(self.vrstica, 0, self.m1[self.vrstica])
            self.preveri()
            if self.preveri_konec():
                self.vrstica = 5
            else:
                self.ponastavi_barve()
                self.odstrani_label
                self.pripni_menu(self.vrstica+1, self.m1[self.vrstica])
        self.vrstica += 1
        if self.vrstica == 6:
            self.odstrani_menu()
            self.konec_igre()

root = Tk()
root.title("Pikice")
app = Pikice(root)
root.mainloop()
