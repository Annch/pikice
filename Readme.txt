PIKICE

Pikice oz Memo oz Mastermind je klasicna igra iz leta 1972. Primarno je bila igra namenjena dvema igralcema, ki sta sledila naslednjim pravilom:
- 1. igralec si med 6 mo�nimi barvami izmisli zaporednje 4 barv, kjer se barve lahko ponavljajo (tj. vzorec)
- 2. igralec posku�a ugotoviti kombinacijo, ki si jo je zamislil 1. igralec
- ko 2. igralec postavi svoj poskus mu 1. igralec odgovori na nacin:
  - crna barva pomeni, da je ena od barv 2. igralca tocno na tistem mestu, kjer je tudi v vzorcu
  - bela barva pomeni, da je ena od barv 2. igralca tudi v sestavljenem vzorcu
Igra se konca, ko 2. igralec ugotovi vzorec ali pa v 6 poskusih ni ugotovil zami�ljene barvne kombinacije 1. igralca.